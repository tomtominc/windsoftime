//Generated from Assets/Create/Scriptable Objects/Update Scriptable Objects Menu

using UnityEditor;

public static class GeneratedScriptableObjectMenu
{
	[MenuItem ("Assets/Create/Scriptable Objects/BehaviourTreeEditor")]
	static void CreateBehaviourTreeEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Panda.BehaviourTreeEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenPathInspector")]
	static void CreateDOTweenPathInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.DOTweenPathInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenVisualManagerInspector")]
	static void CreateDOTweenVisualManagerInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.DOTweenVisualManagerInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/ABSAnimationInspector")]
	static void CreateABSAnimationInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.Core.ABSAnimationInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenInspector")]
	static void CreateDOTweenInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.DOTweenInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenSettingsInspector")]
	static void CreateDOTweenSettingsInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.DOTweenSettingsInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenSettings")]
	static void CreateDOTweenSettings()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.Tweening.Core.DOTweenSettings>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/VectorObject2DEditor")]
	static void CreateVectorObject2DEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<VectorObject2DEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/Configuration")]
	static void CreateConfiguration()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.Configuration>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/EndSpriteAnimationAssetNameEdit")]
	static void CreateEndSpriteAnimationAssetNameEdit()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Animation.EndSpriteAnimationAssetNameEdit>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SpriteAnimationAssetInspector")]
	static void CreateSpriteAnimationAssetInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Animation.SpriteAnimationAssetInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SpriteAnimationEditor")]
	static void CreateSpriteAnimationEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Animation.SpriteAnimationEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GameObjectGUIDsInspector")]
	static void CreateGameObjectGUIDsInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<GameObjectGUIDsInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GridManagerEditor")]
	static void CreateGridManagerEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.GridManagerEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PreRuntimePoolItemInspector")]
	static void CreatePreRuntimePoolItemInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<PreRuntimePoolItemInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SpawnPoolInspector")]
	static void CreateSpawnPoolInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<SpawnPoolInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/ObjectBoundsEditor")]
	static void CreateObjectBoundsEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Tools.ObjectBoundsEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BmEffectEditor")]
	static void CreateBmEffectEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<BlendModes.BmEffectEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BmMaterialEditor")]
	static void CreateBmMaterialEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<BmMaterialEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/CameraOverlayEditor")]
	static void CreateCameraOverlayEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<BlendModes.CameraOverlayEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DOTweenAnimationInspector")]
	static void CreateDOTweenAnimationInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<DG.DOTweenEditor.DOTweenAnimationInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SceneLoaderInspector")]
	static void CreateSceneLoaderInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<SceneLoaderInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UIButtonInspector")]
	static void CreateUIButtonInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UIButtonInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UIEffectInspector")]
	static void CreateUIEffectInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UIEffectInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UIElementInspector")]
	static void CreateUIElementInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UIElementInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UIManagerInspector")]
	static void CreateUIManagerInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UIManagerInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UINotificationInspector")]
	static void CreateUINotificationInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UINotificationInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UITriggerInspector")]
	static void CreateUITriggerInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UITriggerInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/UpdateSortingLayerNameInspector")]
	static void CreateUpdateSortingLayerNameInspector()
	{
		ScriptableObjectMenu.CreateScriptableObject<UpdateSortingLayerNameInspector>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GLEditor`1")]
	static void CreateGLEditor`1()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Extensions.Editor.Internal.GLEditor`1>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/CairoTileGridEditor")]
	static void CreateCairoTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.CairoTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DiamondTileGridEditor")]
	static void CreateDiamondTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.DiamondTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/FlatHexTileGridEditor")]
	static void CreateFlatHexTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.FlatHexTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/FlatRhombTileGridEditor")]
	static void CreateFlatRhombTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.FlatRhombTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/FlatTriTileGridEditor")]
	static void CreateFlatTriTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.FlatTriTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GLEditor`1")]
	static void CreateGLEditor`1()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Obsolete.Grids.Editor.Internal.GLEditor`1>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/LineTileGridEditor")]
	static void CreateLineTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.LineTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/MeshGridEditor`2")]
	static void CreateMeshGridEditor`2()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.MeshGridEditor`2>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PointListTileGridEditor")]
	static void CreatePointListTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PointListTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PointyHexMeshGridEditor")]
	static void CreatePointyHexMeshGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PointyHexMeshGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/FlatHexMeshGridEditor")]
	static void CreateFlatHexMeshGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.FlatHexMeshGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/RectMeshGridEditor")]
	static void CreateRectMeshGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.RectMeshGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DiamondMeshGridEditor")]
	static void CreateDiamondMeshGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.DiamondMeshGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PointyHexTileGridEditor")]
	static void CreatePointyHexTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PointyHexTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PointyRhombTileGridEditor")]
	static void CreatePointyRhombTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PointyRhombTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PointyTriTileGridEditor")]
	static void CreatePointyTriTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PointyTriTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PolarFlatBrickTileGridEditor")]
	static void CreatePolarFlatBrickTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PolarFlatBrickTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PolarPointyBrickTileGridEditor")]
	static void CreatePolarPointyBrickTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PolarPointyBrickTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PolarRectTileGridEditor")]
	static void CreatePolarRectTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.PolarRectTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/RectTileGridEditor")]
	static void CreateRectTileGridEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.RectTileGridEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SimpleGridEditor`2")]
	static void CreateSimpleGridEditor`2()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.SimpleGridEditor`2>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SpriteCellEditor")]
	static void CreateSpriteCellEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Gamelogic.Grids.Editor.Internal.SpriteCellEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PandaBehaviourEditor")]
	static void CreatePandaBehaviourEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Panda.PandaBehaviourEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/CameraBehaviorEditor")]
	static void CreateCameraBehaviorEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<MoreMountains.InfiniteRunnerEngine.CameraBehaviorEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GameManagerEditor")]
	static void CreateGameManagerEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<MoreMountains.InfiniteRunnerEngine.GameManagerEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/LevelManagerEditor")]
	static void CreateLevelManagerEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<MoreMountains.InfiniteRunnerEngine.LevelManagerEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SceneViewIconEditor")]
	static void CreateSceneViewIconEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Tools.SceneViewIconEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/AIBehaviourDatabase")]
	static void CreateAIBehaviourDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.AIBehaviourDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BaseAIBehaviourDatabase")]
	static void CreateBaseAIBehaviourDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.BaseAIBehaviourDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BaseEnemyBlueprintDatabase")]
	static void CreateBaseEnemyBlueprintDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.BaseEnemyBlueprintDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BaseSkillBlueprintDatabase")]
	static void CreateBaseSkillBlueprintDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.BaseSkillBlueprintDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/EnemyBlueprintDatabase")]
	static void CreateEnemyBlueprintDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.EnemyBlueprintDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SkillBlueprintDatabase")]
	static void CreateSkillBlueprintDatabase()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.SkillBlueprintDatabase>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/BaseDatabase`1")]
	static void CreateBaseDatabase`1()
	{
		ScriptableObjectMenu.CreateScriptableObject<RutCreate.LightningDatabase.BaseDatabase`1>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/SpriteAnimationAsset")]
	static void CreateSpriteAnimationAsset()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.Animation.SpriteAnimationAsset>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/GroupContainer")]
	static void CreateGroupContainer()
	{
		ScriptableObjectMenu.CreateScriptableObject<Framework.Core.GroupContainer>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/PixelPerfectCameraEditor")]
	static void CreatePixelPerfectCameraEditor()
	{
		ScriptableObjectMenu.CreateScriptableObject<PixelPerfectCameraEditor>();
	}

	[MenuItem ("Assets/Create/Scriptable Objects/DoozyUI_Data")]
	static void CreateDoozyUI_Data()
	{
		ScriptableObjectMenu.CreateScriptableObject<DoozyUI_Data>();
	}

}

