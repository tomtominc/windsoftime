﻿using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseDatabase<T> : ScriptableObject where T : BaseClass
	{

		[SerializeField]
		protected List<T> m_Items = new List<T>();

		[SerializeField]
		[HideInInspector]
		protected int m_LastID = 0;

		public virtual void Add(T item)
		{
			item.ID = ++m_LastID;
			m_Items.Add(item);
		}

		public virtual void Remove(T item)
		{
			m_Items.Remove(item);
		}

		public virtual void Remove(int id)
		{
			m_Items.RemoveAll(item => item.ID == id);
		}

		public virtual void Remove(Predicate<T> match)
		{
			m_Items.RemoveAll(match);
		}

		public virtual T Find(int id)
		{
			return m_Items.Find(item => item.ID == id);
		}

		public virtual T Find(string name)
		{
			return m_Items.Find(item => item.Name == name);
		}

		public virtual T Find(Predicate<T> match)
		{
			return m_Items.Find(match);
		}

		public virtual List<T> FindAll()
		{
			return m_Items;
		}

		public virtual List<T> FindAll(int id)
		{
			return m_Items.FindAll(item => item.ID == id);
		}

		public virtual List<T> FindAll(string name)
		{
			return m_Items.FindAll(item => item.Name == name);
		}

		public virtual List<T> FindAll(Predicate<T> match)
		{
			return m_Items.FindAll(match);
		}

		public virtual int Count(Predicate<T> match = null)
		{
			return (match == null) ? m_Items.Count : FindAll(match).Count;
		}
	}
}
