﻿using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public class BaseClass
	{
		#region Fields

		[SerializeField]
		protected int m_ID;

		public virtual int ID
		{
			get { return m_ID; }
			set { m_ID = value; }
		}

		[SerializeField]
		protected string m_Name;

		public virtual string Name
		{
			get { return m_Name; }
			set { m_Name = value; }
		}

		#endregion
	}

}
