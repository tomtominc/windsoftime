﻿using UnityEngine;
using System.Collections;
using RutCreate.LightningDatabase;
using UnityEditor;

namespace Framework.Core
{
    [FieldInfo ("Skill Type", "Custom", typeof(SkillType) , "SkillType", "SkillType.Physical" ) ]
    public class SkillTypeField : FieldType
	{
        public override object DrawField(object item)
        {
            SkillType value = (item == null) ? SkillType.Physical : (SkillType)item;
            value = (SkillType)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return SkillType.Physical;
        }
	}

    [FieldInfo ("Element Type", "Custom", typeof(ElementType) , "ElementType", "ElementType.Normal" ) ]
    public class ElementTypeField : FieldType
    {
        public override object DrawField(object item)
        {
            ElementType value = (item == null) ? ElementType.Normal : (ElementType)item;
            value = (ElementType)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return ElementType.Normal;
        }
    }
}