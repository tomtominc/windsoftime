﻿using UnityEngine;
using System.Collections;

public enum SkillType 
{
    Physical, Special, Status 
}

public enum ElementType
{
    Normal, Fire, Water, Ground
}