﻿using UnityEngine;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public class Configuration : ScriptableObject
	{

		public List<Project> Projects = new List<Project>();
	}
}
