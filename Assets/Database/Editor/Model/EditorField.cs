﻿using System;


namespace RutCreate.LightningDatabase
{
	public class EditorField : Field
	{
		public bool IsNew;

		public EditorField(string name, Type type, string editorType = "", bool isList = false, bool isNew = true) : base(name, type, isList, editorType)
		{
			IsNew = isNew;
		}

		public EditorField(EditorField source) : base(source)
		{
			IsNew = source.IsNew;
		}
	}
}
