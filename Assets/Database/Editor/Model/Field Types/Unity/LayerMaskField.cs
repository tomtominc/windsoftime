﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("LayerMask", "Unity", typeof(LayerMask), "LayerMask", "new LayerMask()")]
	public class LayerMaskField : FieldType
	{
		public override object DrawField(object item)
		{
			LayerMask layerMask = (item == null) ? new LayerMask() : (LayerMask)item;
			string[] layers = UnityEditorInternal.InternalEditorUtility.layers;
			List<int> layerNumbers = new List<int>();

			for (int i = 0; i < layers.Length; i++)
			{
				layerNumbers.Add(LayerMask.NameToLayer(layers[i]));
			}

			int maskWithoutEmpty = 0;
			for (int i = 0; i < layerNumbers.Count; i++)
			{
				if (((1 << layerNumbers[i]) & layerMask.value) > 0)
				{
					maskWithoutEmpty |= (1 << i);
				}
			}

			maskWithoutEmpty = UnityEditor.EditorGUILayout.MaskField(maskWithoutEmpty, layers);

			int mask = 0;
			for (int i = 0; i < layerNumbers.Count; i++)
			{
				if ((maskWithoutEmpty & (1 << i)) > 0)
				{
					mask |= (1 << layerNumbers[i]);
				}
			}
			layerMask.value = mask;

			return layerMask;
		}

		public override object GetDefaultValue()
		{
			return new LayerMask();
		}
	}
}
