﻿using UnityEngine;
using UnityEditor;
using System.Linq;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Bounds", "Unity", typeof(Bounds), "Bounds", "new Bounds()")]
	public class BoundsField : FieldType
	{
		public override object DrawField(object item)
		{
			Bounds bounds = (item == null) ? new Bounds() : (Bounds)item;
			item = EditorGUILayout.BoundsField(bounds);
			return item;
		}

		public override object GetDefaultValue()
		{
			return new Bounds();
		}
	}
}
