﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Vector2", "Unity", typeof(Vector2), "Vector2", "Vector2.zero")]
	public class Vector2Field : FieldType
	{
		public override object DrawField(object item)
		{
			Vector2 vector = (item == null) ? Vector2.zero : (Vector2)item;
			item = EditorGUILayout.Vector2Field(GUIContent.none, vector, GUILayout.MinWidth(0f), GUILayout.Height(EditorGUIUtility.singleLineHeight));
			return item;
		}

		public override object GetDefaultValue()
		{
			return Vector2.zero;
		}
	}
}
