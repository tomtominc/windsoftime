﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("AnimationCurve", "Unity", typeof(AnimationCurve), "AnimationCurve", "new AnimationCurve()")]
	public class AnimationCurveField : FieldType
	{
		public override object DrawField(object item)
		{
			AnimationCurve animCurve = (item == null) ? new AnimationCurve() : (AnimationCurve)item;
			item = EditorGUILayout.CurveField(animCurve);
			return item;
		}
	}
}
