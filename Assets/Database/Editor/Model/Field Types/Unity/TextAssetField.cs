﻿using UnityEngine;
using UnityEditor;

namespace RutCreate.LightningDatabase
{
    [FieldInfo("Text Asset", "Unity", typeof(TextAsset), "TextAsset", "null")]
    public class TextAssetField : FieldType
    {
        public override object DrawField(object item)
        {
            item = EditorGUILayout.ObjectField ( item == null ? null : (TextAsset)item, typeof(TextAsset),false );
            return item;
        }
    }
}