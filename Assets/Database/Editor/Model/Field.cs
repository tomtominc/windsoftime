// Credit https://bitbucket.org/rotorz/classtypereference-for-unity

using UnityEngine;

using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Field : ISerializationCallbackReceiver
	{
		public string Name;

		private Type m_Type;

		public Type Type
		{
			get { return m_Type; }
			set
			{
				if (value != null && !value.IsClass)
				{
					throw new ArgumentException(string.Format("'{0}' is not a class type.", value.FullName), "value");
				}

				m_Type = value;
				m_ClassRef = GetClassRef(value);
			}
		}

		public static string GetClassRef(Type type)
		{
			return (type != null)
				? type.FullName + ", " + type.Assembly.GetName().Name
					: "";
		}

		[SerializeField]
		private string m_ClassRef;

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (!string.IsNullOrEmpty(m_ClassRef))
			{
				m_Type = System.Type.GetType(m_ClassRef);
				if (m_Type == null)
				{
					Debug.LogWarningFormat("'{0}' was referenced but class type was not found.", m_ClassRef);
				}
			}
			else
			{
				m_Type = null;
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		public bool IsList;

		public string EditorType;

		public float ColumnWidth = 100f;

		public string FormattedType
		{
			get
			{ 
				string formattedType = EditorUtil.GetFieldTypeString(this.Type);
				if (IsList)
				{
					formattedType = string.Format("List<{0}>", formattedType);
				}

				return formattedType;
			}
		}

		public string DefaultValue
		{
			get
			{
				string defaultValue = string.Empty;

				if (IsList)
				{
					defaultValue = string.Format("new {0}()", FormattedType);
				}
				else
				{
					defaultValue = EditorUtil.GetFieldTypeDefaultValue(this.Type);
				}

				return defaultValue;
			}
		}

		public string CName
		{
			get
			{
				return Name.ToUppercaseFirst();
			}
		}

		public Field(string name, Type type, bool isList = false, string editorType = "")
		{
			// TODO
			// Validate type. It needs to derive from FieldType.
			Name = name;
			Type = type;
			IsList = isList;
			EditorType = editorType;
		}

		public Field(Field source)
		{
			Name = source.Name;
			Type = source.Type;
			IsList = source.IsList;
			EditorType = source.EditorType;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
