﻿using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public partial class LightningWindow
	{
		private const float ToggleColumnWidth = 20f;
		private const float StatusBarHeight = 34f;
		private const float ScrollbarSize = 15f;
		private const float TableHeaderHeight = 19f;
		private const float TableHeaderBorderWidth = 1f;
		private const float TableRowHeight = 20f;
		private const float TableRowSpacing = 2f;
		private const float TableColumnPadding = 1f;
		private const float TableFooterHeight = 25f;
		private const float TableMarginTop = 4f;

		private const float MinColumnWidth = 50f;
		private const float ResizeColumnHandleSize = 10f;

		private bool m_IsResizingColumn = false;
		private int m_ResizingColumnIndex = -1;
		private float m_ResizingStartWidth = 0f;
		private float m_ResizingStartMousePosition = 0f;
		private float m_TableWidth = 0f;

		private int[] m_ItemsPerPageValues = new int[] { 10, 20, 50, 100 };
		private string[] m_ItemsPerPageOptions;

		private List<int> m_SelectionIndices = new List<int>();
		private bool m_SelectAll = false;

		private void DrawBrowserTable()
		{
			if (m_DataManager == null) return;

			m_DataManager.StartRecordUndo();
			DrawBrowserTableHeader();
			DrawBrowserTableData();
			DrawBrowserTableFooter();
			m_DataManager.StopRecordUndo();
		}

		private void DrawBrowserTableHeader()
		{
			Rect rect = new Rect();
			rect.x = InitialSidebarWidth;
			rect.width = position.width - InitialSidebarWidth;
			rect.y = StatusBarHeight + TableMarginTop;
			rect.height = TableHeaderHeight;

			// Reserve layout area.
			GUILayoutUtility.GetRect(rect.width, rect.height);

			GUI.BeginGroup(rect);

			Rect columnPosition = new Rect();
			columnPosition.x = -Mathf.Clamp(m_BrowserScrollPosition.x, 0f, float.MaxValue); // Move along the vertical scrollbar.
			columnPosition.y = TableHeaderBorderWidth;
			columnPosition.width = ToggleColumnWidth;
			columnPosition.height = rect.height;

			GUI.Box(columnPosition, "", "OL Title");

			Rect togglePosition = columnPosition;
			togglePosition.x += 3f;
			togglePosition.y += 1f;
			bool selectAll = GUI.Toggle(togglePosition, m_SelectAll, "");
			if (selectAll != m_SelectAll)
			{
				ClearSelection();

				if (selectAll == true)
				{
					for (int index = m_DataManager.StartItemIndex; index <= m_DataManager.EndItemIndex; index++)
					{
						m_SelectionIndices.Add(index);
					}
				}
				m_SelectAll = selectAll;
			}

			for (int i = 0; i < CurrentClass.Fields.Count; i++)
			{
				Field field = CurrentClass.Fields[i];
				columnPosition.x += columnPosition.width;
				columnPosition.width = field.ColumnWidth;

				GUI.Box(columnPosition, field.Name, RCGUIStyles.BrowserTableHeader);

				Rect handleRect = columnPosition;
				handleRect.width = ResizeColumnHandleSize;
				handleRect.x = (columnPosition.x + columnPosition.width) - handleRect.width * 0.5f;
				EditorGUIUtility.AddCursorRect(handleRect, MouseCursor.SplitResizeLeftRight);

				switch (Event.current.type)
				{
					case EventType.MouseDown:
						if (handleRect.Contains(Event.current.mousePosition))
						{
							m_IsResizingColumn = true;
							m_ResizingColumnIndex = i;
							m_ResizingStartWidth = field.ColumnWidth;
							m_ResizingStartMousePosition = Event.current.mousePosition.x + rect.x; // Plus offset from group.
							Event.current.Use();
						}
						break;
				}
			}
			GUI.EndGroup();

			m_TableWidth = columnPosition.xMax;

			if (m_IsResizingColumn && m_ResizingColumnIndex >= 0)
			{
				switch (Event.current.type)
				{
					case EventType.MouseDrag:
						Field field = CurrentClass.Fields[m_ResizingColumnIndex];
						float deltaX = Event.current.mousePosition.x - m_ResizingStartMousePosition;
						field.ColumnWidth = Mathf.Clamp(m_ResizingStartWidth + deltaX, MinColumnWidth, float.MaxValue);
						Repaint();
						Event.current.Use();
						break;
					case EventType.MouseUp:
						m_IsResizingColumn = false;
						Event.current.Use();
						break;
				}
			}
		}

		private void DrawBrowserTableData()
		{
			m_BrowserScrollPosition = EditorGUILayout.BeginScrollView(m_BrowserScrollPosition, GUILayout.Width(position.width - InitialSidebarWidth));
			for (int index = m_DataManager.StartItemIndex; index <= m_DataManager.EndItemIndex; index++)
			{
				if (m_DataManager.List.Count <= index) break;

				object item = m_DataManager.List[index];

				GUIStyle rowStyle = (index % 2 != 0) ? RCGUIStyles.BrowserTableRow : RCGUIStyles.BrowserTableRowStripe;
				EditorGUILayout.BeginHorizontal(rowStyle, GUILayout.MaxWidth(m_TableWidth));

				EditorGUILayout.BeginVertical(RCGUIStyles.BrowserTableCell, GUILayout.Width(ToggleColumnWidth));
				bool isSelected = m_SelectionIndices.Contains(index);
				bool selectedResult = EditorGUILayout.Toggle(isSelected);
				if (selectedResult != isSelected)
				{
					if (selectedResult == true)
					{
						m_SelectionIndices.Add(index);
						if (m_SelectionIndices.Count == (m_DataManager.EndItemIndex - m_DataManager.StartItemIndex + 1))
						{
							m_SelectAll = true;
						}
					}
					else
					{
						m_SelectAll = false;
						m_SelectionIndices.Remove(index);
					}
				}
				EditorGUILayout.EndVertical();

				for (int fieldIndex = 0; fieldIndex < CurrentClass.Fields.Count; fieldIndex++)
				{
					Field field = CurrentClass.Fields[fieldIndex];
					EditorGUILayout.BeginVertical(RCGUIStyles.BrowserTableCell, GUILayout.Width(field.ColumnWidth));
					DrawBrowserTableField(field, item);
					EditorGUILayout.EndVertical();
				}
				GUILayout.FlexibleSpace();
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndScrollView();
		}

		private void DrawBrowserTableField(Field field, object item)
		{
			FieldType fieldType = System.Activator.CreateInstance(field.Type) as FieldType;
			Type itemType = item.GetType();
			PropertyInfo property = itemType.GetProperty(field.Name);
			if (property == null)
			{
				EditorGUILayout.LabelField("Error", GUILayout.MinWidth(0f));
				return;
			}
			object value = property.GetValue(item, null);

			if (Util.IsList(value))
			{
				IList list = value as IList;
				if (list.Count == 0)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("", RCGUIStyles.ButtonPlus))
					{
						list.Add(fieldType.GetDefaultValue());
					}
					GUILayout.FlexibleSpace();
					EditorGUILayout.EndHorizontal();
				}
				else
				{
					int indexToAdd = -1;
					int indexToRemove = -1;
					for (int i = 0; i < list.Count; i++)
					{
						EditorGUILayout.BeginHorizontal();
						list[i] = fieldType.DrawField(list[i]);

						if (GUILayout.Button("", RCGUIStyles.ButtonPlus))
						{
							indexToAdd = i;
						}

						if (GUILayout.Button("", RCGUIStyles.ButtonMinus))
						{
							indexToRemove = i;
						}
						EditorGUILayout.EndHorizontal();
					}

					if (indexToAdd >= 0)
					{
						list.Insert(indexToAdd + 1, fieldType.GetDefaultValue());
					}

					if (indexToRemove >= 0)
					{
						list.RemoveAt(indexToRemove);
					}
				}
			}
			else
			{
				if (field.Name == "ID")
				{
					EditorGUILayout.LabelField(value.ToString(), GUILayout.MinWidth(0f));
				}
				else
				{
					fieldType.DrawField(item as BaseClass, field.Name);
				}
			}
		}

		private void DrawBrowserTableFooter()
		{
			EditorGUILayout.BeginHorizontal(RCGUIStyles.BrowserTableFooter, GUILayout.MaxHeight(TableFooterHeight));

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Add new row", EditorStyles.miniButton))
			{
				m_DataManager.AddItem();
				m_DataManager.LastPage();
			}

			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();

			GUI.enabled = (m_SelectionIndices.Count > 0);
			if (GUILayout.Button("Remove selected", EditorStyles.miniButton))
			{
				if (EditorUtility.DisplayDialog(
					    "Remove selected row(s)",
					    string.Format("Are your sure you want to remove {0} selected row(s)?", m_SelectionIndices.Count),
					    "Remove", "No, keep it"))
				{
					for (int i = m_SelectionIndices.Count - 1; i >= 0; i--)
					{
						int index = m_SelectionIndices[i];
						if (index < m_DataManager.List.Count)
						{
							m_DataManager.List.RemoveAt(index);
						}
					}
					ClearSelection();

					// Need to recalculate total page in case of remove all items in that page.
					m_DataManager.ItemsPerPage = m_DataManager.ItemsPerPage;
				}
			}
			GUI.enabled = m_Editable;

			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUILayout.FlexibleSpace();

			if (m_ItemsPerPageOptions == null || m_ItemsPerPageOptions.Length != m_ItemsPerPageValues.Length)
			{
				m_ItemsPerPageOptions = new string[m_ItemsPerPageValues.Length];
				for (int i = 0; i < m_ItemsPerPageValues.Length; i++)
				{
					m_ItemsPerPageOptions[i] = m_ItemsPerPageValues[i].ToString();
				}
			}

			EditorGUI.BeginChangeCheck();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			int itemsPerPage = EditorGUILayout.IntPopup(m_DataManager.ItemsPerPage, m_ItemsPerPageOptions, m_ItemsPerPageValues, GUILayout.Width(50f));
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			if (EditorGUI.EndChangeCheck())
			{
				m_DataManager.ItemsPerPage = itemsPerPage;
				EditorPrefs.SetInt(CurrentClass.Name + "_IPP", m_DataManager.ItemsPerPage);
				ClearSelection();
			}

			GUI.enabled = (m_DataManager.Page > 1);

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Prev", EditorStyles.miniButton))
			{
				m_DataManager.PreviousPage();
				ClearSelection();
			}
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			string pagingString = string.Format("{0}/{1}", m_DataManager.Page, m_DataManager.TotalPage);
			EditorGUILayout.LabelField(pagingString, GUILayout.Width(50f));
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUI.enabled = (m_DataManager.Page < m_DataManager.TotalPage);

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Next", EditorStyles.miniButton))
			{
				m_DataManager.NextPage();
				ClearSelection();
			}
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUI.enabled = m_Editable;

			EditorGUILayout.EndHorizontal();
		}

		private void ClearSelection()
		{
			m_SelectionIndices.Clear();
			m_SelectAll = false;
		}
	}
}
