﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public static class EditorUtil
	{
		public static bool IsValidClassName(string name)
		{
			if (string.IsNullOrEmpty(name)) return false;
			if (!char.IsLetter(name.ToCharArray()[0])) return false;
			if (!Util.IsValidCSharpVariableName(name)) return false;
			return true;
		}

		public static string GetFieldTypeName(Type type)
		{
			FieldInfoAttribute fieldInfoAttribute = GetFieldInfoAttribute(type);
			if (fieldInfoAttribute != null)
			{
				return fieldInfoAttribute.Name;
			}
			return type.Name;
		}

		public static string GetFieldTypeString(Type type)
		{
			FieldInfoAttribute fieldInfoAttribute = GetFieldInfoAttribute(type);
			if (fieldInfoAttribute != null)
			{
				return fieldInfoAttribute.TypeString;
			}
			return type.Name;
		}

		public static Type GetFieldTypeType(Type type)
		{
			FieldInfoAttribute fieldInfoAttribute = GetFieldInfoAttribute(type);
			if (fieldInfoAttribute != null)
			{
				return fieldInfoAttribute.Type;
			}
			return null;
		}

		public static string GetFieldTypeDefaultValue(Type type)
		{
			FieldInfoAttribute fieldInfoAttribute = GetFieldInfoAttribute(type);
			if (fieldInfoAttribute != null)
			{
				return fieldInfoAttribute.DefaultValue;
			}
			return string.Empty;
		}

		public static string GetFieldTypeRequiredNamespace(Type type)
		{
			FieldInfoAttribute fieldInfoAttribute = GetFieldInfoAttribute(type);
			if (fieldInfoAttribute != null)
			{
				return fieldInfoAttribute.RequiredNamespace;
			}
			return string.Empty;
		}

		public static FieldInfoAttribute GetFieldInfoAttribute(Type type)
		{
			foreach (object attr in type.GetCustomAttributes(false))
			{
				FieldInfoAttribute fieldInfoAttribute = attr as FieldInfoAttribute;
				return fieldInfoAttribute;
			}
			return null;
		}

		public static Type GetLightningFieldTypeFromType(Type type)
		{
			List<Type> fieldTypes = Util.FindDerivedTypes(typeof(FieldType)).ToList();
			foreach (Type fieldType in fieldTypes)
			{
				if (type == GetFieldTypeType(fieldType))
				{
					return fieldType;
				}
			}
			return null;
		}

		private static float m_PreviousLabelWidth = 16f;

		public static void SetLabelWidth(float width)
		{
			m_PreviousLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = width;
		}

		public static void ResetLabelWidth()
		{
			EditorGUIUtility.labelWidth = m_PreviousLabelWidth;
		}
	}
}
