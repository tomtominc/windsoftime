﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace RutCreate.LightningDatabase
{
	public class ClassBuilder
	{
		public const string TemplateDirectory = "Template";
		public const string TemplateDirectoryDefault = "Default";

		public const string TemplateFileModel = "Model.cs.template";
		public const string TemplateFileBaseModel = "BaseModel.cs.template";
		public const string TemplateFileDatabase = "Database.cs.template";
		public const string TemplateFileBaseDatabase = "BaseDatabase.cs.template";

		public const string OutputDirectory = "Databases";
		public const string OutputDatabaseDirectory = "Database";
		public const string OutputScriptDirectory = "Scripts";

		private string m_ModelScriptContent;
		private string m_BaseModelScriptContent;
		private string m_DatabaseScriptContent;
		private string m_BaseDatabaseScriptContent;

		private Class m_Class;

		public static void Build(Class klass)
		{
			ClassBuilder builder = new ClassBuilder(klass);
			builder.Build();
		}

		public static void Delete(Class klass)
		{
			ClassBuilder builder = new ClassBuilder(klass);
			builder.Delete();
		}

		public ClassBuilder(Class klass)
		{
			m_Class = klass;
			PrepareTemplate();
		}

		public void Build()
		{
			PrepareDirectories();
			CreateScripts();

			AssetDatabase.Refresh();
		}

		public void Delete()
		{
			string baseModelPath = Path.Combine(OutputScriptPath, string.Format("Base{0}.cs", m_Class.Name));
			if (File.Exists(baseModelPath))
			{
				File.Delete(baseModelPath);
			}

			string baseDatabasePath = Path.Combine(OutputScriptPath, string.Format("Base{0}Database.cs", m_Class.Name));
			if (File.Exists(baseDatabasePath))
			{
				File.Delete(baseDatabasePath);
			}

			string modelPath = Path.Combine(OutputScriptPath, string.Format("{0}.cs", m_Class.Name));
			if (File.Exists(modelPath))
			{
				File.Delete(modelPath);
			}

			string databasePath = Path.Combine(OutputScriptPath, string.Format("{0}Database.cs", m_Class.Name));
			if (File.Exists(databasePath))
			{
				File.Delete(databasePath);
			}

			AssetDatabase.Refresh();
		}

		private void CreateScripts()
		{
			File.WriteAllText(Path.Combine(OutputScriptPath, string.Format("Base{0}.cs", m_Class.Name)), m_BaseModelScriptContent);
			File.WriteAllText(Path.Combine(OutputScriptPath, string.Format("Base{0}Database.cs", m_Class.Name)), m_BaseDatabaseScriptContent);

			string modelPath = Path.Combine(OutputScriptPath, string.Format("{0}.cs", m_Class.Name));
			if (!File.Exists(modelPath))
			{
				File.WriteAllText(modelPath, m_ModelScriptContent);
			}

			string databasePath = Path.Combine(OutputScriptPath, string.Format("{0}Database.cs", m_Class.Name));
			if (!File.Exists(databasePath))
			{
				File.WriteAllText(databasePath, m_DatabaseScriptContent);
			}
		}

		private void PrepareTemplate()
		{
			PrepareTemplateModel();
			PrepareTemplateBaseModel();
			PrepareTemplateDatabase();
			PrepareTemplateBaseDatabase();
		}

		private void PrepareTemplateModel()
		{
			m_ModelScriptContent = GetGeneratedFormattedTemplate(TemplateFileModel, new Dictionary<string, object> {
				{ "ClassName", m_Class.Name }
			});
		}

		private void PrepareTemplateBaseModel()
		{
			List<Dictionary<string, object>> fieldsToken = new List<Dictionary<string, object>>();
			StringBuilder requiredNamespaces = new StringBuilder();
			foreach (Field field in m_Class.Fields)
			{
				if (field.Name == "ID" || field.Name == "Name") continue;

				Dictionary<string, object> fieldToken = new Dictionary<string, object> {
					{ "Name", field.Name },
					{ "Type", field.FormattedType },
					{ "DefaultValue", field.DefaultValue },
					{ "CName", field.CName }
				};
				fieldsToken.Add(fieldToken);

				string requiredNamespace = EditorUtil.GetFieldTypeRequiredNamespace(field.Type);
				if (!string.IsNullOrEmpty(requiredNamespace))
				{
					requiredNamespaces.AppendFormat("using {0};\n", requiredNamespace);
				}
			}

			m_BaseModelScriptContent = GetGeneratedFormattedTemplate(TemplateFileBaseModel, new Dictionary<string, object> {
				{ "ClassName", m_Class.Name },
				{ "Fields", fieldsToken },
				{ "RequiredNamespaces", requiredNamespaces.ToString() }
			});
		}

		private void PrepareTemplateDatabase()
		{
			m_DatabaseScriptContent = GetGeneratedFormattedTemplate(TemplateFileDatabase, new Dictionary<string, object> {
				{ "ClassName", m_Class.Name }
			});
		}

		private void PrepareTemplateBaseDatabase()
		{
			m_BaseDatabaseScriptContent = GetGeneratedFormattedTemplate(TemplateFileBaseDatabase, new Dictionary<string, object> {
				{ "ClassName", m_Class.Name }
			});
		}

		private string GetGeneratedFormattedTemplate(string filename, Dictionary<string, object> tokens)
		{
			StringBuilder sb = new StringBuilder();

			string templatePath = Path.Combine(Path.GetDirectoryName(WorkingDirectory), TemplateDirectory);
			templatePath = Path.Combine(templatePath, TemplateDirectoryDefault);
			templatePath = Path.Combine(templatePath, filename);

			if (!File.Exists(templatePath))
			{
				Debug.LogErrorFormat("Could file template name: {0}", filename);
				return string.Empty;
			}

			using (StreamReader reader = new StreamReader(templatePath, Encoding.Default))
			{
				bool foundForeach = false;
				string foreachItemsKey = string.Empty;
				string foreachItemKey = string.Empty;
				StringBuilder foreachContent = new StringBuilder();

				while (reader.Peek() >= 0)
				{
					string line = reader.ReadLine();
					if (line == null) continue;
					string trimmedLine = line.Trim();

					if (trimmedLine.ToLower().StartsWith("{{foreach"))
					{
						foundForeach = true;
						foreachContent.Length = 0;

						string[] chunks = trimmedLine.Substring(2, trimmedLine.Length - 4).Split(' ');
						foreachItemsKey = chunks[chunks.Length - 1];
						foreachItemKey = chunks[1];
						continue;
					}

					if (foundForeach)
					{
						if (trimmedLine.ToLower() == "{{endforeach}}")
						{
							if (tokens.ContainsKey(foreachItemsKey))
							{
								bool isList = tokens[foreachItemsKey].GetType().GetGenericTypeDefinition() == typeof(List<>);
								if (isList)
								{
									string content = foreachContent.ToString();
									List<Dictionary<string, object>> items = tokens[foreachItemsKey] as List<Dictionary<string, object>>;
									for (int i = 0; i < items.Count; i++)
									{
										Dictionary<string, object> itemWithKey = new Dictionary<string, object>();
										foreach (KeyValuePair<string, object> entry in items[i])
										{
											itemWithKey.Add(string.Format("{0}.{1}", foreachItemKey, entry.Key), entry.Value);
										}
										itemWithKey.Add("Index", i);
										itemWithKey.Add("Row", i + 1);

										sb.AppendLine(content.BetterFormat(itemWithKey));
									}
								}
							}

							foundForeach = false;
						}
						else
						{
							foreachContent.AppendLine(line);
						}

						continue;
					}

					line = line.BetterFormat(tokens);
					sb.AppendLine(line);
				}
			}

			return sb.ToString();
		}

		private void PrepareDirectories()
		{
			if (!Directory.Exists(OutputPath))
			{
				Directory.CreateDirectory(OutputPath);
			}

			if (!Directory.Exists(OutputDatabasePath))
			{
				Directory.CreateDirectory(OutputDatabasePath);
			}

			if (!Directory.Exists(OutputScriptPath))
			{
				Directory.CreateDirectory(OutputScriptPath);
			}
		}

		public static string WorkingDirectory
		{
			get
			{
				string projectPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar;
				string filepath = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
				string directory = Path.GetDirectoryName(filepath.Substring(projectPath.Length));
				return directory;
			}
		}

		public static string RootAssetDirectory
		{
			get
			{
				string rootDirectory = Directory.GetParent(Directory.GetParent(WorkingDirectory).ToString()).ToString();
				return rootDirectory;
			}
		}

		public static string OutputPath
		{
			get
			{
				string outputPath = Path.Combine(RootAssetDirectory, OutputDirectory);
				return outputPath;
			}
		}

		public static string OutputDatabasePath
		{
			get
			{
				string outputPath = Path.Combine(OutputPath, OutputDatabaseDirectory);
				return outputPath;
			}
		}

		public static string OutputScriptPath
		{
			get
			{
				string outputPath = Path.Combine(OutputPath, OutputScriptDirectory);
				return outputPath;
			}
		}
	}
}
