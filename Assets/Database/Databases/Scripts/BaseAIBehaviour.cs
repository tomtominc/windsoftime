using UnityEngine;
using System.Collections.Generic;
using Framework.Core;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseAIBehaviour : BaseClass
	{

		[SerializeField]
		protected TextAsset m_Behaviour = null;

		public virtual TextAsset Behaviour
		{
			get { return m_Behaviour; }
			set { m_Behaviour = value; }
		}

	}
}
