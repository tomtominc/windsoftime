using UnityEngine;
using System.Collections.Generic;
using Framework.Core;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseAutonomousBlueprint : BaseClass
	{

		[SerializeField]
		protected ElementType m_Element = ElementType.Normal;

		public virtual ElementType Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}

		[SerializeField]
		protected Vector2 m_LookDirection = Vector2.zero;

		public virtual Vector2 LookDirection
		{
			get { return m_LookDirection; }
			set { m_LookDirection = value; }
		}

		[SerializeField]
		protected Vector2 m_StepLimit = Vector2.zero;

		public virtual Vector2 StepLimit
		{
			get { return m_StepLimit; }
			set { m_StepLimit = value; }
		}

		[SerializeField]
		protected bool m_Teleport = false;

		public virtual bool Teleport
		{
			get { return m_Teleport; }
			set { m_Teleport = value; }
		}

		[SerializeField]
		protected string m_AIBehaviourID = string.Empty;

		public virtual string AIBehaviourID
		{
			get { return m_AIBehaviourID; }
			set { m_AIBehaviourID = value; }
		}

		[SerializeField]
		protected List<string> m_SpawnContainer = new List<string>();

		public virtual List<string> SpawnContainer
		{
			get { return m_SpawnContainer; }
			set { m_SpawnContainer = value; }
		}

	}
}
