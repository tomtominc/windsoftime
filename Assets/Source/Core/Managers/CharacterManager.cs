﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Framework.Core
{
    public enum CharacterKey 
    {
        Knight, Blade, Cat
    }

    [System.Serializable]
    public class CharacterData
    {
        [SerializeField]
        public CharacterKey key;

        [SerializeField]
        public int level;

        public CharacterData Clone ()
        {
            return new CharacterData () { key = key, level = level };
        }
    }


    public class CharacterManager : MonoBehaviour , ILoader 
	{
        [SerializeField]
        private float _priority = 1f;

        public float Priority { get { return _priority; } }

        private const string LOAD_KEY = "CHARACTER_DATA_LOAD_KEY_{0}";

        private Dictionary < CharacterKey , CharacterData > _characterDatabase;

        public IEnumerator Load ()
        {
            _characterDatabase = new Dictionary < CharacterKey , CharacterData > ();

            foreach ( CharacterKey key in Enum < CharacterKey >.ToValues () )
            {
                CharacterData data = PersistentManager.Load < CharacterData > 
                    ( string.Format (LOAD_KEY, key ) , new CharacterData () { key = key } );
                
                _characterDatabase.Add ( key , data );
            }

            yield break;
        }

        public void ModifyData ( CharacterKey key , CharacterData value )
        {
            if ( _characterDatabase.ContainsKey ( key ) )
            {
                _characterDatabase [ key ] = value.Clone ();
                SaveData ( key , value );
            }
        }

        public void SaveData ( CharacterKey key , CharacterData value )
        {
            PersistentManager.Save < CharacterData > ( string.Format ( LOAD_KEY, key ), value );
        }

        public void SaveAllData ()
        {
            foreach ( var pair in _characterDatabase )
            {
                SaveData ( pair.Key, pair.Value );
            }
        }

        private void OnApplicationQuit ()
        {
            
        }

	}
}