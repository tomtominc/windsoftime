﻿using UnityEditor;

namespace Framework.Core
{
	public partial class GLMenu
	{
		[MenuItem("Gamelogic/Grids/API Documentation")]
		public static void OpenGridsAPI()
		{
			OpenUrl("http://www.gamelogic.co.za/documentation/grids/");
		}
	}
}