﻿Shader "BlendModes/MeshDefault/Framebuffer" 
{
	Properties 
	{
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	SubShader 
	{
		Tags 
		{ 
			"Queue" = "Transparent" 
			"RenderType" = "Transparent" 
		}
		
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		
		Pass 
		{  
			CGPROGRAM
			
			#include "UnityCG.cginc"
			#include "../BlendModes.cginc"

			#pragma only_renderers framebufferfetch
			#pragma target 3.0			
			#pragma multi_compile BmDarken BmMultiply BmColorBurn BmLinearBurn BmDarkerColor BmLighten BmScreen BmColorDodge BmLinearDodge BmLighterColor BmOverlay BmSoftLight BmHardLight BmVividLight BmLinearLight BmPinLight BmHardMix BmDifference BmExclusion BmSubtract BmDivide BmHue BmSaturation BmColor BmLuminosity
			#pragma vertex ComputeVertex
			#pragma fragment ComputeFragment
			
			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;

			struct VertexInput 
			{
				float4 Vertex : POSITION;
				float2 TexCoord : TEXCOORD0;
			};

			struct VertexOutput 
			{
				float4 Vertex : SV_POSITION;
				float2 TexCoord : TEXCOORD0;
			};
			
			VertexOutput ComputeVertex(VertexInput vertexInput)
			{
				VertexOutput vertexOutput;
				
				vertexOutput.Vertex = mul(UNITY_MATRIX_MVP, vertexInput.Vertex);
				vertexOutput.TexCoord = TRANSFORM_TEX(vertexInput.TexCoord, _MainTex);
				
				return vertexOutput;
			}

			void ComputeFragment (VertexOutput vertexOutput, inout fixed4 bufferColor : SV_Target)
			{
			    fixed4 texColor = tex2D(_MainTex, vertexOutput.TexCoord) * _Color;
				fixed4 grabColor = bufferColor;
				
				#include "../BlendOps.cginc"

				bufferColor = blendResult;
			} 
			
			ENDCG
		}
	}
	
	FallBack "Diffuse"
	CustomEditor "BmMaterialEditor"
}
