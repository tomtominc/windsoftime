Shader "BlendModes/SpriteDefault/UnifiedGrab"
{
	Properties
	{
		[PerRendererData] 
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		[MaterialToggle] 
		PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue" = "Transparent" 
			"IgnoreProjector" = "True" 
			"RenderType" = "Transparent" 
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		GrabPass { "_BmSharedGT" }
		
		Pass
		{
			CGPROGRAM
			
			#include "UnityCG.cginc"
			#include "../BlendModes.cginc"

			#pragma target 3.0			
			#pragma multi_compile BmDarken BmMultiply BmColorBurn BmLinearBurn BmDarkerColor BmLighten BmScreen BmColorDodge BmLinearDodge BmLighterColor BmOverlay BmSoftLight BmHardLight BmVividLight BmLinearLight BmPinLight BmHardMix BmDifference BmExclusion BmSubtract BmDivide BmHue BmSaturation BmColor BmLuminosity
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#pragma vertex ComputeVertex
			#pragma fragment ComputeFragment
			
			sampler2D _MainTex;
			sampler2D _BmSharedGT;
			fixed4 _Color;
			
			struct VertexInput
			{
				float4 Vertex : POSITION;
				fixed4 Color : COLOR;
				float2 TexCoord : TEXCOORD0;
			};

			struct VertexOutput
			{
				float4 Vertex : SV_POSITION;
				fixed4 Color : COLOR;
				float2 TexCoord : TEXCOORD0;
				float4 ScreenPos : TEXCOORD1;
			};
			
			VertexOutput ComputeVertex (VertexInput vertexInput)
			{
				VertexOutput vertexOutput;
				
				vertexOutput.Vertex = mul(UNITY_MATRIX_MVP, vertexInput.Vertex);
				vertexOutput.ScreenPos = vertexOutput.Vertex;	
				vertexOutput.TexCoord = vertexInput.TexCoord;
				vertexOutput.Color = vertexInput.Color * _Color;
				#ifdef PIXELSNAP_ON
				vertexOutput.Vertex = UnityPixelSnap(vertexOutput.Vertex);
				#endif
							
				return vertexOutput;
			}
			
			fixed4 ComputeFragment (VertexOutput vertexOutput) : SV_Target
			{
				half4 texColor = tex2D(_MainTex, vertexOutput.TexCoord) * vertexOutput.Color;
				//texColor.rgb *= texColor.a;
				
				float2 grabTexCoord = vertexOutput.ScreenPos.xy / vertexOutput.ScreenPos.w; 
				grabTexCoord.x = (grabTexCoord.x + 1.0) * .5;
				grabTexCoord.y = (grabTexCoord.y + 1.0) * .5; 
				#if UNITY_UV_STARTS_AT_TOP
				grabTexCoord.y = 1.0 - grabTexCoord.y;
				#endif
				
				fixed4 grabColor = tex2D(_BmSharedGT, grabTexCoord); 
				
				#include "../BlendOps.cginc"

				return blendResult;
			}
			
			ENDCG
		}
	}
	
	Fallback "Sprites/Default"
	CustomEditor "BmMaterialEditor"
}
