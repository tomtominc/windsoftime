﻿Shader "BlendModes/ParticleDefault/Framebuffer" 
{
	Properties 
	{
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Particle Texture", 2D) = "white" {}
	}

	Category 
	{
		Tags 
		{ 
			"Queue" = "Transparent" 
			"IgnoreProjector" = "True" 
			"RenderType" = "Transparent" 
		}
		
		AlphaTest Greater .01
		ColorMask RGB
		Cull Off 
		Lighting Off 
		ZWrite Off 
		Fog { Color (0,0,0,0) }
		Blend SrcAlpha OneMinusSrcAlpha
		
		SubShader 
		{
			Pass 
			{
				CGPROGRAM
			
				#include "UnityCG.cginc"
				#include "../BlendModes.cginc"

				#pragma only_renderers framebufferfetch
				#pragma target 3.0				
				#pragma multi_compile BmDarken BmMultiply BmColorBurn BmLinearBurn BmDarkerColor BmLighten BmScreen BmColorDodge BmLinearDodge BmLighterColor BmOverlay BmSoftLight BmHardLight BmVividLight BmLinearLight BmPinLight BmHardMix BmDifference BmExclusion BmSubtract BmDivide BmHue BmSaturation BmColor BmLuminosity
				#pragma vertex ComputeVertex
				#pragma fragment ComputeFragment

				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _Color;
				
				struct VertexInput 
				{
					float4 Vertex : POSITION;
					float2 TexCoord : TEXCOORD0;
				};

				struct VertexOutput 
				{
					float4 Vertex : SV_POSITION;
					float2 TexCoord : TEXCOORD0;
				};

				VertexOutput ComputeVertex(VertexInput vertexInput)
				{
					VertexOutput vertexOutput;
					
					vertexOutput.Vertex = mul(UNITY_MATRIX_MVP, vertexInput.Vertex);
					vertexOutput.TexCoord = TRANSFORM_TEX(vertexInput.TexCoord, _MainTex);
					
					return vertexOutput;
				}
				
				void ComputeFragment (VertexOutput vertexOutput, inout fixed4 bufferColor : SV_Target)
				{
				    fixed4 texColor = tex2D(_MainTex, vertexOutput.TexCoord) * _Color;
					fixed4 grabColor = bufferColor;
					
					#include "../BlendOps.cginc"

					bufferColor = blendResult;
				} 
				
				ENDCG 
			}
		}	
	}
	
	FallBack "Particles/Additive"
	CustomEditor "BmMaterialEditor"
}
