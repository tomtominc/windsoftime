﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Framework.Core.Tools
{	
	public class HiddenAttribute : PropertyAttribute { }
}
